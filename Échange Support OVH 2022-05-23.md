**Sujet :** Migration non sollicitée    
**Service :** mxplan-5gkw61o-1

#### **De :** Moi › **Date de réception** : 23/05/2022 09:04 

>Adresse e-mail concernée :
>_adresse1@mondns.net_ et _adresse2@mondns.net_
>
>Message d'erreur :
>Connexion IMAP en erreur depuis clients de messagerie
>
>Informations complémentaires :
>
>Madame, Monsieur,
>
>Je vous écris ce jour pour vous faire part de mon profond mécontentement : depuis vendredi j'ai des problèmes de connexion IMAP à répétition sur "adresse1@mondns.net" depuis mes deux clients de messagerie (Android et Thunderbird).  
>  
>Vendredi je n'arrivais même pas à me connecter au Webmail. J'ai réussi samedi et j'ai eu la très désagréable surprise de constater que j'avais basculé sur Outlook (avec très certainement Exchange derrière) en lieu et place de Roundcube.  
>  
>Quand m'avez-vous demandé ou, à minima, notifié pour une telle migration ?  
>Est-il possible de revenir à une infrastucture plus ouverte ?  
>Pouvez-vous me dire à quoi sont dus ces problèmes de connexion IMAP ?  
>  
>Merci par avance.  
>Franck ALBARET

#### **De :** Support OVH › **Date de réception** : 25/05/2022 11:39 

>Je me permets de prendre en charge votre demande d'assistance.
>
>Suite à une vérification je constate que l'adresse email adresse1@mondns.net
>
>L'adresse mail est active et votre domaine pointe bien vers les serveurs mails d'OVHcloud.
>
>Je vous invite de changer votre mot de passe par un nouveau de votre choix pour résoudre le problème.
>Voici la documentation concernant la modification de mot de passe :
>
>https://docs.ovh.com/fr/emails/modifier-mot-de-passe-adresse-email/
>
>
>Je vous invite à tester l'accès depuis le webmail :
>
>https://webmail.mail.ovh.net
>Si vous rencontrez le même souci veuillez vider le cache de votre navigateur. Si il persiste, je vous invite à tester l'accès au webmail depuis un autre navigateur.
>
>N'hésitez pas à revenir vers moi avec des captures d'écran du dysfonctionnement.

#### **De :** Support OVH › **Date de réception** : 25/05/2022 11:40 

>Je me permets de prendre en charge votre demande d'assistance.
>
>Suite à une vérification je constate que l'adresse email adresse1@mondns.net
>
>L'adresse mail est active et votre domaine pointe bien vers les serveurs mails d'OVHcloud.
>
>Je vous invite de changer votre mot de passe par un nouveau de votre choix pour résoudre le problème.
>Voici la documentation concernant la modification de mot de passe :
>
>https://docs.ovh.com/fr/emails/modifier-mot-de-passe-adresse-email/
>
>
>Je vous invite à tester l'accès depuis le webmail :
>
>https://webmail.mail.ovh.net
>Si vous rencontrez le même souci veuillez vider le cache de votre navigateur. Si il persiste, je vous invite à tester l'accès au webmail depuis un autre navigateur.
>
>N'hésitez pas à revenir vers moi avec des captures d'écran du dysfonctionnement.

#### **De :** Support OVH › **Date de réception** : 29/05/2022 00:01 

>Il y a trois jours, nous vous avons sollicité pour un complément d'information concernant votre demande. Nous n'avons pas encore reçu un retour de votre part. Veuillez noter qu'en l'absence de retour, cette demande sera considérée comme résolue dans quatre jours.

#### **De :** Moi › **Date de réception** : 29/05/2022 15:41 

>Bonjour,
>
>Je viens de modifier mon mot de passe comme indiqué, et je vous dirai après quelques jours si cela corrige le problème de connexion intempestif.
>
>Maintenant, concernant la migration vers Exchange / Outlook, y'a-t-il un quelconque moyen de revenir en arrière ou du moins   vers un autre solution "non Microsoft" et plus open source comme auparavant ?
>
>Merci par avance.
>
>Cordialement,  
>Franck ALBARET

#### **De :** Support OVH › **Date de réception** : 30/05/2022 09:30 

>Suite à une vérification, pour votre compte adresse1@mondns.net je ne vois pas que vous avez fait une migration vers exchange vous n'avez pas  une platforme exchange .
>Je suis navrée, mais votre demande n'est pas assez claire.
>
>Afin que je puisse vous aider, merci de me fournir plus de détails.

#### **De :** Moi › **Date de réception** : 30/05/2022 16:28 

>Pourtant, en essayant de me connecter sur l'adresse https://mail.ovh.net/roundcube/ comme auparavant je suis redirigé sur Outlook avec la mention " Connecté à Exchange " en bas de page, je pense que c'est suffisamment parlant (à moins que ce soit un mauvais affichage de OWA).
>
>A dire vrai, je ne sais pas quand la migration Roundcube vers Outlook a été faite, je sais juste que :
>- avant j'avais un webmail Roundcube
>- je me connectais jusqu'à présent via IMAP depuis mes clients
>- j'ai des problèmes de connexion IMAP depuis vendredi 20 mai
>
>Je me répète mais je n'ai pas consenti à cette migration, je ne désire pas utiliser de produit Microsoft et je veux juste savoir si chez vous (OVH), il a une quelconque possibilité d'utiliser une autre infrastructure mail (?)
>
>Autre chose, depuis mon changement de mot de passe comme vous me l'avez indiqué, j'ai perdu tout accès en IMAP, ça ne veut plus se connecter via Thunderbird et le client mail d'Android.

#### **De :** Support OVH › **Date de réception** : 31/05/2022 10:13 

>Pour accéder au webmail ovh je vous invite d'accéder via ce lien :
>
>https://webmail.mail.ovh.net
>
>Ce n'est pas un outil Microsoft, et je vous assure que vous être en train d'utiliser nos outils.
>
>Si une erreur venait à surgir, n'hésitez pas à revenir vers moi avec des captures d'écran du dysfonctionnement.
>Veuillez poster les captures sur l'outil "Plik" d'OVH, et me renvoyer leurs liens d'accès.
>
>L'outil Plik est accessible depuis ce lien :
>
>https://plik.ovhcloud.com/#/

#### **De :** Moi › **Date de réception** : 31/05/2022 11:24 

>Merci mais je suis bien au fait du nouveau lien, c'était juste pour "historique" et bien montrer que j'utilisais une autre solution avant ; ici le problème n'est pas le lien d'accès au Webmail mais le client utilisé et le problème de connexion qui en résulte, Rouncube n'était pas parfait mais l'IMAP fonctionnait sans soucis … aucun problème depuis 2015 que je suis chez vous.
>
>Jusqu'à preuve du contraire, Outlook ou Exchange sont des produits Microsoft (très étonné par votre réponse), que vous les installiez sur vos infrastructures grand bien vous en fasse mais ça n'en reste pas moins "vos outils", votre paramétrage à minima.
>
>Pour l'IMAP, je ne sais si la capture d'écran sera d'une grande plu-value (ça me dit juste que la connexion est impossible) mais ici ce que j'utilise comme paramètres :  
>[ Réception ] Serveur : ssl0.ovh.net / Port : 993 / Sécurité : SSL / TLS  
>[ Envoi ] Serveur : ssl0.ovh.net / Port : 465 / Sécurité : SSL / TLS  
>Avec mon adresse en utilisateur et le mot de passe dernièrement modifié
>
>Merci par avance

#### **De :** Support OVH › **Date de réception** : 01/06/2022 11:37 

>Si une erreur persiste ou venait à surgir de nouveau, n'hésitez pas à revenir vers moi avec des captures d'écran du dysfonctionnement.
>
>Afin de me permettre de vous communiquer la solution adéquate
>
>Veuillez poster les captures sur l'outil "Plik" d'OVH, et me renvoyer leurs liens d'accès.
>
>L'outil Plik est accessible depuis ce lien :
>
>https://plik.ovhcloud.com/#/
>
>Je reste à votre disposition pour tout renseignement complémentaire.

#### **De :** Moi › **Date de réception** : 02/06/2022 12:04 

>Copier-coller tout à fait inacceptable de votre part, je demande à minima d'échanger avec un(e) humain(e) et non de répondre à un scénario scripté.
>
>Je peux effectivement vous transmettre des captures d'écran de mes deux clients de messagerie indiquant que la connexion ne se fait pas mais je ne vois pas en quoi cela va faire avancer mon problème. Des logs peut-être mais des captures d'écran j'ai un doute.
>
>Je vous demande à minima ici de bien valider les paramètres utilisés :
>
>[ IMAP Réception ] Serveur : ssl0.ovh.net - Port : 993 - Sécurité : SSL / TLS  
>[ SMTP Envoi ] Serveur : ssl0.ovh.net - Port : 465 - Sécurité : SSL / TLS
>
>Avec pour les deux "adresse1@mondns.net" en nom d'utilisateur et mon mot de passe redéfini récemment.
>
>Rien n'a changé de ce point de vue ?  
>Pas de double authentification (2FA) ?  
>Tous les caractères UTF-8 sont-ils bien acceptés pour le mot de passe ?
>
>Si vous n'êtes pas à même de répondre, merci de transférer le ticket au niveau supérieur du support.
>
>Cordialement,  
>Franck ALBARET

#### **De :** Support OVH › **Date de réception** : 02/06/2022 18:08 

>Suite aux vérifications depuis nos outils de travail, je constate que L'opération de changement mot de passe a été terminé avec sucée.
>Le souci est rencontré à quel niveau ?
>Dans l'attente de votre retour.
>
>Si tout fonctionne depuis le Webmail, cela veut dire que votre service e-mail est opérationnel. Dans ce cas, le dysfonctionnement ne provient pas du compte mail lui-même, mais du logiciel utilisé.
>
>Voici les paramètres à utiliser pour configurer votre client de messagerie:
>
>• Entrant:  
>Serveur: ssl0.ovh.net / Protocole: IMAP / Port : 993 / Sécurité: SSL-TLS  
>ou  
>Serveur: ssl0.ovh.net / Protocole: POP / Port : 995/ Sécurité: SSL-TLS
>
>• Sortant:  
>Serveur: ssl0.ovh.net / Protocole: SMTP / Port : 465 / Sécurité: SSL-TLS
>
>Pour avoir plus de détails, je vous invite à consulter ce guide :  
>https://docs.ovh.com/fr/emails/configuration-outlook-2016/

#### **De :** Support OVH › **Date de réception** : 06/06/2022 00:00 

>Il y a trois jours, nous vous avons sollicité pour un complément d'information concernant votre demande. Nous n'avons pas encore reçu un retour de votre part. Veuillez noter qu'en l'absence de retour, cette demande sera considérée comme résolue dans quatre jours.

#### **De :** Moi › **Date de réception** : 09/06/2022 10:58 

>Bonjour,
>
>Merci tout d'abord pour l'approbation des paramètres.
>
>Ensuite, j'ai solutionné mon problème de connexion à l'IMAP en testant plusieurs choses et finalement en changeant mon mot de passe qui avait UN caractère problématique (pourtant UTF-8). Donc de ce point de vue, je n'ai plus de problème.
>
>Par contre subsiste le problème du webmail Outlook, est-il possible de revenir à (ou migrer vers) une solution plus ouverte et non Microsoft ?  
>Dois-je changer d'hébergeur ?
>
>Cordialement,  
>Franck ALBARET

#### **De :** Support OVH › **Date de réception** : 09/06/2022 18:06 

>Voici les offres e-mail proposé par OVH :
>
>Les adresses emails mutualisés (liées à un hébergement), je tiens à vous préciser que la taille de stockage d'une adresse e-mail mutualisée ne dépasse pas les 5 Go.
>
>L'offre E-mail Pro, proposée à 0.99€/HT par mois. Elle vous permet de doubler votre quota en disposant de 10 Go de stockage. Vous trouverez plus d'informations sur cette offre à l'adresse suivante :  
>https://www.ovh.com/fr/emails/email-pro/
>
>L'offre Exchange (Hosted et Private), proposée à partir de 2.99€/HT par mois. Vous pouvez choisir ici entre 50 Go et 300 Go. Le service Exchange propose également d'autres fonctionnalités. Rendez-vous sur la page Exchange 2016 afin de découvrir les offres et choisir celle qui est la mieux adaptée à votre besoin :  
>https://www.ovh.com/fr/emails/hosted-exchange/
>
>Pour plus d'informations, vous pouvez consulter le guide de migration disponible à l'adresse suivante :  
>https://docs.ovh.com/fr/microsoft-collaborative-solutions/migration-adresse-e-mail-mutualisee-vers-exchange/#32-migrer-la-nouvelle-version-du-mxplan_1

#### **De :** Moi › **Date de réception** : 10/06/2022 08:34 

>Bonjour,
>
>Je suis un peu effaré par votre réponse. Je suis en train de vous dire que JE NE VEUX PAS utiliser de produits Microsoft, que je veux revenir à minima sur le webmail précédent (Roundcube) ou autre solution open source.
>
>Ici tout ce que vous me proposez sont deux offres :
>- Offre E-mail Pro fonctionnant avec Outlook Web Access (de Microsoft)
>- Offre Exchange (de Microsoft)
>
>Donc vous me confirmez que chez OVH, dorénavant, il n'y a pas d'autre possibilité que d'utiliser du Microsoft ?  
>Malgré les discours de souveraineté numérique ?
>
>Je me répète peut-être encore mais, au delà des réponses scriptées que vous devez avoir, si ce que je vous dis ne vous parle pas, je ne suis pas contre échanger avec quelqu'un·e de plus technique.
>
>Cordialement,  
>Franck ALBARET

#### **De :** Support OVH › **Date de réception** : 10/06/2022 17:44 

>Je tiens à vous préciser que toute l'équipe OVH et moi même y attachons une grande importance, et vous pouvez être assurés que nous ferons de notre mieux pour coopérer et trouver de bonnes solutions.
>
>Je viens de lancer une demande de vérification auprès de nos administrateurs.
>
>Je vous prie de bien vouloir patienter, je m'en charge personnellement du suivi et je reviendrai vers vous dès que possible.

#### **De :** Support OVH › **Date de réception** : 13/06/2022 23:53 

>Cette demande est actuellement considérée comme étant résolue et nous sommes en attente de la confirmation de la solution que nous vous avons proposé.
>
>Nous vous rappelons qu'afin de confirmer la résolution de votre demande, il suffit soit de répondre à ce mail en indiquant « Accepté » dans la première ligne de votre réponse, soit de clore l'échange dans l'espace client.
>
>Si vous souhaitez rejeter la solution proposée, vous pouvez soit apporter des précisions en réponse à ce mail en indiquant « Rejeté » dans la première ligne, soit poursuivre l'échange en cours dans l'espace client.
>
>En l'absence d'une action de votre part, votre demande d'assistance sera fermée automatiquement dans quatre jours.

#### **De :** Moi › **Date de réception** : 14/06/2022 09:23 

>Rejeté
>
>Bonjour,
>
>Toujours dans l'attente d'un webmail alternatif à Outlook et d'un retour des administrateurs (comme indiqué ci-dessous).
>
>Cordialement,  
>Franck ALBARET

#### **De :** Support OVH › **Date de réception** : 15/06/2022 12:15 

>Je reviens vers vous dans le cadre du suivi de votre demande.
>Navrée de vous informer que nous ne pouvons pas changer l'infrastructure de votre service email.
>Je vous informe que votre service email est sur la meilleure infrastructure disponible actuellement par OVH.
> 
>Si vous rencontrez un souci merci de m'indiquer, il est rencontré à quel niveau ?
>
>Je reste à votre disposition pour tout renseignement complémentaire.

#### **De :** Support OVH › **Date de réception** : 18/06/2022 23:36 

>Cette demande est actuellement considérée comme étant résolue et nous sommes en attente de la confirmation de la solution que nous vous avons proposé.
>
>Nous vous rappelons qu'afin de confirmer la résolution de votre demande, il suffit soit de répondre à ce mail en indiquant « Accepté » dans la première ligne de votre réponse, soit de clore l'échange dans l'espace client.
>
>Si vous souhaitez rejeter la solution proposée, vous pouvez soit apporter des précisions en réponse à ce mail en indiquant « Rejeté » dans la première ligne, soit poursuivre l'échange en cours dans l'espace client.
>
>En l'absence d'une action de votre part, votre demande d'assistance sera fermée automatiquement dans quatre jours.

#### **De :** Moi › **Date de réception** : 22/06/2022 08:36 

>Bonjour,
>
>Je vois que je n'ai pas le choix, en tout cas chez vous.
>
>Je me tue à vous dire que c'est plus ici un choix d'outil qu'un problème technique, et que la pluralité des services offerts par OVH s'en trouve amoindrie.
>
>Profondément mécontent, je vais faire une croix sur une collaboration de près de 7 ans et la prochaine demande que vous recevrez de ma part sera pour transférer mes noms de domaine, d'ici à Mars 2023 date de mon renouvellement ; je vais commencer par migrer mon VPS.
>
>Au revoir.
>
>Franck ALBARET

#### **De :** Moi › **Date de réception** : 22/06/2022 10:05 

>PS : Je discute avec pas mal de webmestres qui ont un ou plusieurs hébergements chez vous dont leur messagerie.
>
>Encore plusieurs d'entre eux sont encore sur Roundcube donc je sais que :
>- l'infrastucture, l'outil existe donc toujours chez vous
>- nous avons donc bien ici un REFUS de votre part de me voir accéder à cet outil
>- ces mêmes webmestres migreront également ailleurs lorsqu'Outlook leur sera imposé
>
>Bien comprendre que mes remontrances ne s'adressent pas en particulier à la personne qui s'est chargée de mon ticket (qui a fait ce qu'elle a pu je pense) mais bien aux choix techniques des administrateurs et dirigeants d'OVH qui semblent privilégier des orientations et partenariats commerciaux semble-t-il juteux au détriment de ce qui a fait le succès d'OVH : le souplesse dans le choix des outils.
>
>Jusqu'ici je faisais votre promotion systématiquement, il n'en sera plus question.

#### **De :** Support OVH › **Date de réception** : 23/06/2022 12:08 

>Je suis navrée que la réponse apportée ne vous satisfasse pas.
>
>Je vous remercie cependant pour votre compréhension.
